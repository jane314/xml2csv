# xml2csv

A simple tool to convert XML to CSV.

[Link to webpage.](https://jane314.gitlab.io/xml2csv/xml2csv)

[Explanation of syntax.](https://gitlab.com/jane314/xml2csv/-/blob/master/syntax_explanation.pdf)

No syntax checking yet! Syntax errors or XML errors means the script won't run, generally. But this shouldn't be sensitive to XML nodes missing attributes, unlike some other parsers!
