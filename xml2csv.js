/*	
	xml2csv
	Valentine's Day #2021
	Jane Davis (https://gitlab.com/jane314)
*/

// global variables
const textarea_syntax = document.getElementById('textarea_syntax');
const textarea_xml = document.getElementById('textarea_xml');
const input_csv_separator = document.getElementById('input_csv_separator');
const dp = new DOMParser();

// initial GUI box values
textarea_syntax.value = 'name column;root.folder!.person!.data:type:name\ncity column;root.folder!.person!.data:type:city';
textarea_xml.value = '<root><folder><person><data type="name">Lila</data><data type="city">Madison</data></person><person><data type="name">Marcello</data><data type="city">Fond du Lac</data></person></folder><folder><person><data type="name">Elena</data><data type="city">Prarie du Chien</data></person><person><data type="name">Michele</data><data type="city">Janesville</data></person></folder></root>';

// iterate xml_node according to the instructions in syntax_tree_node.
// returns an a single XML node or array of XML nodes
function iterate_xml_node( xml_node, syntax_tree_node ) {
	if( syntax_tree_node.type == 'multiple' ) {
		return [...xml_node.getElementsByTagName(syntax_tree_node.node_str)];
	} else if ( syntax_tree_node.type == 'single' ) {
		return xml_node.getElementsByTagName(syntax_tree_node.node_str)[0];
	} else if ( syntax_tree_node.type == 'keyvalue' ) {
		return [...xml_node.getElementsByTagName(syntax_tree_node.node_str)]
			.filter( z => z.getAttribute(syntax_tree_node.value1) == syntax_tree_node.value2 )[0];
	} else { // keyvalue_multiple
		return [...xml_node.getElementsByTagName(syntax_tree_node.node_str)]
			.filter( z => z.getAttribute(syntax_tree_node.value1) == syntax_tree_node.value2 );
	}
}

// parses an xml_node into a csv column according to the instructions in syntax_tree 
function parse_xml_node_into_column( xml_node, syntax_tree ) {
	let iter = xml_node;
	for( let i = 0; i < syntax_tree.length; i++ ) {
		if( Array.isArray(iter) ) {
			iter = iter.map( z => iterate_xml_node( z, syntax_tree[i]	) )
				.map( z => { if ( !z ) { return { textContent: '' }; } else { return z; } } )
				.flat();
		} else {
			iter = iterate_xml_node( iter, syntax_tree[i] );
		} 
	}
	return iter.map( z => z.textContent.replace( /^\s*/, '' ).replace( /\s*$/, '' ) ); // last part shouldn't be necessary?
}

// parses node path syntax into a syntax tree
function parse_node_path_syntax( syntax ) {
	return syntax.split('\n')
	.filter( str => !( /^\s*$/.test(str) ) )
	.map( str => {
		const str_split = str.split(';');
		return {
			col_name: str_split[0],
			tree: str_split[1]
				.split('.')
				.map( node_str => {
					if( /(?<node_str>.*):(?<value1>.*):(?<value2>.*)!$/.test( node_str ) ) {
						const match = node_str.match( /(?<node_str>.*):(?<value1>.*):(?<value2>.*)!$/ );
						return {
							node_str: match.groups['node_str'],
							value1: match.groups['value1'],
							value2: match.groups['value2'],
							type: 'keyvalue_multiple'
						};
					} else if( /(?<node_str>.*):(?<value1>.*):(?<value2>.*)/.test( node_str ) ) {
						const match = node_str.match( /(?<node_str>.*):(?<value1>.*):(?<value2>.*)/ );
						return {
							node_str: match.groups['node_str'],
							value1: match.groups['value1'],
							value2: match.groups['value2'],
							type: 'keyvalue'
						};
					} else if ( /!$/.test(node_str) ) {
						return {
							node_str: node_str.split('!')[0],
							type: 'multiple'
						};
					} else {
						return {
							node_str: node_str,
							type: 'single'
						};
					}
				} )
		}
	} );
}

// compute the output csv file from the xml and syntax tree
function refresh() {
	const xml = dp.parseFromString(textarea_xml.value, 'text/xml');
	const textarea_output = document.getElementById('textarea_output');
	const col_list = parse_node_path_syntax(textarea_syntax.value);
	const grid_aux = col_list.map( y => parse_xml_node_into_column(xml,y.tree));
	const n = grid_aux[0].length;
	const csv_sep = input_csv_separator.value;
	textarea_output.value = col_list.map( col => col.col_name ).join(csv_sep) 
		+ '\n' 
		+ [...new Array(n).keys()]
			.map( t => grid_aux.map( arr => arr[t] ).join(csv_sep) )
			.join('\n');
}

refresh();
